(function () {
	var iPay = {
		init: function () {
			try {
				iPay.handlers.place();
			} catch (err) {
				console.error('[ERROR] '+err);
			}
		},
		events: {
			validate_login: function (event) {
				var $login = $('#user_login').val(),
					$passwd = $('#user_password').val();

				if ($login === '' || $passwd === '') {
					$('.login .alert').fadeIn();
					return false;
				}
			},
			payment: function () {
				var $parent = $(this),
					$modal = $('#myModal'),
					desc = $parent.find('.desc').text(),
					value = $parent.find('.value').text(),
					category = $parent.find('.category').text(),
					owner = $parent.find('.owner').text(),
					expiredate = $parent.find('.date').text(),
					docnumber = '', docdate = '',
					date = new Date();

				docdate = date.getDate()+'/'+('0'+(date.getMonth()+1)).slice(-2)+'/'+date.getFullYear();

				for (var i = 0; i < 8; i++) {
					docnumber += iPay.handlers.pattern();
				}

				docnumber = docnumber.replace(/^(\d{5})(\d{5})(\d{5})(\d{6})(\d{5})(\d{6})(\d{1})(\d{6})$/gi, '$1.$2.$3.$4 $5.$6 $7 $8');
				docnumber = docnumber+value.replace(',', '');

				$modal.find('.modal-docnumber').text(docnumber);
				$modal.find('.modal-docdate').text(docdate);
				$modal.find('.modal-docdate2').text(docdate);
				$modal.find('.modal-title').text(desc);
				$modal.find('.modal-desc').text(desc);
				$modal.find('.modal-value').text('R$'+value);
				$modal.find('.modal-owner').text(owner);
				$modal.find('.modal-date').text(expiredate);

				$modal.modal();
			}
		},
		handlers: {
			place: function () {
				$('.login form').on('submit', iPay.events.validate_login);
				$('.payable-accounts table tr').on('click', iPay.events.payment);
			},
			pattern: function () {
				return Math.floor(Math.random() * 100000);
			}
		}
	}
	iPay.init();
})();